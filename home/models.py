from django.db import models

# Create your models here.
class ScheduleModel(models.Model):
	name = models.CharField(max_length=50)

	DAY_CHOICES = ('monday', 'Monday'), ('tuesday', 'Tuesday'), ('wednesday', 'Wednesday'), ('thursday', 'Thursday'), ('friday', 'Friday'), ('saturday', 'Saturday'), ('sunday', 'Sunday')
	day = models.CharField(max_length=50, choices=DAY_CHOICES)

	date = models.DateField()
	time = models.TimeField()

	CATEGORY_CHOICES = ('task', 'Task'), ('quiz', 'Quiz'), ('other', 'Other')
	category = models.CharField(max_length=50, choices=CATEGORY_CHOICES)

	location = models.CharField(max_length=50)

	def __str__(self):
		return self.name