from django import forms
from .models import ScheduleModel

class scheduleForm(forms.ModelForm):

	class Meta:
		model = ScheduleModel
		fields = ('name', 'day', 'date', 'time', 'category','location')