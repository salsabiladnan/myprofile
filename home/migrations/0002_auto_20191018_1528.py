# Generated by Django 2.1.4 on 2019-10-18 08:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ScheduleModel',
            new_name='Snippet',
        ),
    ]
