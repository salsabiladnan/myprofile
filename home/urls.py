from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('education/', views.education, name='education'),
    path('skills/', views.skills, name='skills'),
    path('experiences/', views.experiences, name='experiences'),
    path('contact/', views.contact, name='contact'),
    path('schedule/', views.schedule, name='schedule'),
]
