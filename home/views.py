from django.shortcuts import render
from django.http import HttpResponse
from .forms import scheduleForm

# Create your views here.
def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def education(request):
    return render(request, 'education.html')

def skills(request):
    return render(request, 'skills.html')

def experiences(request):
    return render(request, 'experiences.html')

def contact(request):
    return render(request, 'contact.html')

def schedule(request):
	if request.method == "POST":
		form = scheduleForm(request.POST)
		if form.is_valid():
			form.save()
	form = scheduleForm()
	return render(request, 'form.html', {'form': form})