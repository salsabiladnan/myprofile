from django import forms
class Message_Form(forms.Form):

    name = forms.CharField(label='Nama', required=False,
                           max_length=27, empty_value='Anonymous',
                           widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(required=False,
                             widget=forms.EmailInput(attrs=attrs))
    message = forms.CharField(widget=forms.Textarea(attrs=attrs),
                              requires=True)
